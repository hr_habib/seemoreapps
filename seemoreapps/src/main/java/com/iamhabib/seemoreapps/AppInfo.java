package com.iamhabib.seemoreapps;

import java.io.Serializable;

/**
 * Created by HABIB on 4/25/2017.
 */

public class AppInfo implements Serializable{
    private String appName, appUrl, logoUrl;
    private int ratingProgress;

    public AppInfo(String appName, String appUrl, String logoUrl, int ratingProgress) {
        this.appName = appName;
        this.appUrl = appUrl;
        this.logoUrl = logoUrl;
        this.ratingProgress = ratingProgress;
    }

    public AppInfo(String appName, String appUrl, String logoUrl) {
        this.appName = appName;
        this.appUrl = appUrl;
        this.logoUrl = logoUrl;
        this.ratingProgress=0;
    }

    public AppInfo(String appName, String appUrl) {
        this.appName = appName;
        this.appUrl = appUrl;
        this.logoUrl = "";
        this.ratingProgress=0;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppUrl() {
        return appUrl;
    }

    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public int getRatingProgress() {
        return ratingProgress;
    }

    public void setRatingProgress(int ratingProgress) {
        this.ratingProgress = ratingProgress;
    }
}
