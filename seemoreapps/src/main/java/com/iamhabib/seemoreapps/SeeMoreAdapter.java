package com.iamhabib.seemoreapps;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * Created by HABIB on 4/25/2017.
 */

public class SeeMoreAdapter extends BaseAdapter {

    Activity context;
    private List<AppInfo> appList;

    public SeeMoreAdapter(Activity context, List<AppInfo> appList) {
        this.context = context;
        this.appList=appList;
    }

    @Override
    public int getCount() {
        return appList.size();
    }

    @Override
    public Object getItem(int position) {
        return appList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView= LayoutInflater.from(context).inflate(R.layout.see_more_apps_view, parent, false);
        }

        final AppInfo appInfo=appList.get(position);

        ImageView iv_app_logo=(ImageView) convertView.findViewById(R.id.iv_app_logo);
        TextView tv_app_title=(TextView) convertView.findViewById(R.id.tv_app_title);
        MaterialRatingBar mrb_app_rating=(MaterialRatingBar) convertView.findViewById(R.id.mrb_app_rating);
        Button btn_install=(Button) convertView.findViewById(R.id.btn_install);
        Glide.with(context)
                .load(appInfo.getLogoUrl())
                .placeholder(R.drawable.app_logo)
                .crossFade()
                .into(iv_app_logo);
        tv_app_title.setText(appInfo.getAppName());
        tv_app_title.setSelected(true);
        mrb_app_rating.setProgress(appInfo.getRatingProgress());
        btn_install.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appInfo.getAppUrl())));
            }
        });
        return convertView;
    }
}
