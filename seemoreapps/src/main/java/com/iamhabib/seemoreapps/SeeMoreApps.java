package com.iamhabib.seemoreapps;

import android.app.Activity;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HABIB on 4/25/2017.
 */

public class SeeMoreApps {
    private static int DAYS_AFTER_PROMPT = 1;
    private static final String ALREADY_LAUNCHED_PREF = "already_launched";
    private static final String LAST_LAUNCHED_TIME_PREF = "last_launched_time";
    private static final String PREF_NAME = "com.iamhabib.seemoreapps";

    private static AlertDialog dialog;

    private static SharedPreferences prefs;
    private static SharedPreferences.Editor editor;
    private static boolean canLaunch = false;

    public static SeeMoreApps with(Activity activity, String json) {
        prefs = activity.getSharedPreferences(PREF_NAME, 0);
        editor = prefs.edit();

        Long last_launched = prefs.getLong(LAST_LAUNCHED_TIME_PREF, 0);

        if (System.currentTimeMillis() >= last_launched +
                (DAYS_AFTER_PROMPT * 24 * 60 * 60 * 1000)) {
            editor.putBoolean(ALREADY_LAUNCHED_PREF, false);
        }

        if (last_launched == 0) {
            canLaunch = true;
            last_launched = System.currentTimeMillis();
            editor.putLong(LAST_LAUNCHED_TIME_PREF, last_launched);
            build(activity, json);
        } else if (System.currentTimeMillis() >= last_launched +
                (DAYS_AFTER_PROMPT * 24 * 60 * 60 * 1000)) {
            if (!prefs.getBoolean(ALREADY_LAUNCHED_PREF, false)) {
                canLaunch = true;
                build(activity, json);
                editor.putLong(LAST_LAUNCHED_TIME_PREF, last_launched);
                editor.putBoolean(ALREADY_LAUNCHED_PREF, true);
            } else {
                canLaunch = false;
            }
        } else {
            editor.putBoolean(ALREADY_LAUNCHED_PREF, false);
            canLaunch = false;
        }


        editor.commit();

        return new SeeMoreApps();
    }

    public static SeeMoreApps with(Activity activity, int daysAfterPrompt, String json) {
        DAYS_AFTER_PROMPT = daysAfterPrompt;
        return with(activity, json);
    }

    public static SeeMoreApps build(Activity activity, String json) {
        AppInfo[] list = new Gson().fromJson(json, AppInfo[].class);
        List<AppInfo> infoList = new ArrayList<>();
        for (AppInfo a : list) {
            infoList.add(a);
        }

        return show(activity, infoList);
    }

    public static SeeMoreApps with(Activity activity, List<AppInfo> infoList) {
        prefs = activity.getSharedPreferences(PREF_NAME, 0);
        editor = prefs.edit();

        Long last_launched = prefs.getLong(LAST_LAUNCHED_TIME_PREF, 0);

        if (System.currentTimeMillis() >= last_launched +
                (DAYS_AFTER_PROMPT * 24 * 60 * 60 * 1000)) {
            editor.putBoolean(ALREADY_LAUNCHED_PREF, false);
        }

        if (last_launched == 0) {
            canLaunch = true;
            last_launched = System.currentTimeMillis();
            editor.putLong(LAST_LAUNCHED_TIME_PREF, last_launched);
            show(activity, infoList);
        } else if (System.currentTimeMillis() >= last_launched +
                (DAYS_AFTER_PROMPT * 24 * 60 * 60 * 1000)) {
            if (!prefs.getBoolean(ALREADY_LAUNCHED_PREF, false)) {
                canLaunch = true;
                show(activity, infoList);
                editor.putLong(LAST_LAUNCHED_TIME_PREF, last_launched);
                editor.putBoolean(ALREADY_LAUNCHED_PREF, true);
            } else {
                canLaunch = false;
            }
        } else {
            editor.putBoolean(ALREADY_LAUNCHED_PREF, false);
            canLaunch = false;
        }


        editor.commit();

        return new SeeMoreApps();
    }

    public static SeeMoreApps with(Activity activity, int daysUntilPrompt, List<AppInfo> infoList) {
        DAYS_AFTER_PROMPT = daysUntilPrompt;
        return with(activity, infoList);
    }

    public static SeeMoreApps show(final Activity activity, List<AppInfo> infoList) {

        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.see_more_main, null);

        ListView lv_apps = (ListView) view.findViewById(R.id.lv_see_more);

        SeeMoreAdapter seeMoreAdapter = new SeeMoreAdapter(activity, infoList);
        lv_apps.setAdapter(seeMoreAdapter);

        dialog = new AlertDialog.Builder(activity)
                .setView(view)
                .create();
        dialog.show();

        return new SeeMoreApps();
    }

    public boolean canLaunch() {
        return canLaunch;
    }
}
