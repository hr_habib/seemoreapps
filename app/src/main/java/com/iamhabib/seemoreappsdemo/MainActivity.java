package com.iamhabib.seemoreappsdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.iamhabib.seemoreapps.SeeMoreApps;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onBackPressed() {
        SeeMoreApps a=SeeMoreApps.with(MainActivity.this, getString(R.string.see_more_apps_json));
        if(!a.canLaunch()){
            super.onBackPressed();
        }
    }
}
